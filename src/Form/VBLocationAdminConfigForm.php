<?php

namespace Drupal\vb_location\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityDisplayRepository;

/**
 * Provides a VB Location Admin config form.
 */
class VBLocationAdminConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vb_location_admin_config_form';
  }

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vb_location.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vb_location.settings');

    $entity_display = \Drupal::service('entity_display.repository');
    $view_modes = $entity_display->getViewModeOptionsByBundle('node', 'blog');

    $form['overview_display'] = [
      '#type' => 'select',
      '#title' => $this->t('Overview viewmode'),
      '#options' => $view_modes,
      '#default_value' => $config->get('overview_display') ? $config->get('overview_display') : 'teaser',
      '#description' => $this->t('Viewmode used in the overview. The viewmode must be enabled as view display for the Blog content type to be shown here.')
    ];

    $form['detail_page'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable detail page'),
      '#default_value' => $config->get('detail_page'),
      '#description' => $this->t('Disabling detail pages makes it so teasers aren\'t linked to their detail pages and detail pages are not published')
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->configFactory->getEditable('vb_location.settings')
      ->set('overview_display', $values['overview_display'])
      ->set('detail_page', $values['detail_page'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
